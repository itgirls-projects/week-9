import java.util.*;
import java.util.stream.Collectors;

public class Main {
    // задаем параметры для генерации коллекции и вывода данных:
    static int ageBorders = 35;
    static String separator = "----------";
    static ArrayList<String> animalList = new ArrayList<>();
    static int zooLimit = 10;

    public static void main(String[] args) {
        // создаем банк животных, из которых будут случайным образом выбираться животные для коллекции:
        animalList.add("dodo");
        animalList.add("lion");
        animalList.add("tiger");
        animalList.add("leopard");
        animalList.add("moose");
        animalList.add("gazelle");
        animalList.add("wildcat");
        animalList.add("parrot");
        animalList.add("whale");
        animalList.add("dolphin");
        animalList.add("elk");
        animalList.add("hippo");
        animalList.add("crocodile");
        animalList.add("eagle");
        animalList.add("chicken");
        animalList.add("possum");

        ArrayList<Animal> zoo = new ArrayList<>();
        // случайным образом заполняем коллекцию, границы для возраста и количества животных в ней задаются ранее:
        for (int i = 0; i < zooLimit; i++) {
            zoo.add(new Animal(randomName(animalList), randomAge(ageBorders)));
        }

        // выводим сгенерированную коллекцию:
        System.out.println("INITIAL ZOO LINEUP: ");
        for(Animal animal : zoo) {
            System.out.println(animal.getName() + "; " + animal.getAge() + " years old");
        }
        System.out.println(separator);

        // Если в коллекции есть лев, то выведите его возраст, если нет - выбросите исключение IllegalArgumentException
        try {
            zoo.stream()
                    .filter(animal -> animal.getName().equals("lion"))
                    .forEach(animal -> System.out.println("this lion at the zoo is " + animal.getAge() + " years old"));
            // проверяем на исключение, если в коллекции нет льва:
            if (zoo.stream().noneMatch(animal -> animal.getName().equals("lion"))) {
                throw new IllegalArgumentException();
            }
        } catch(IllegalArgumentException e) {
            System.out.println("no lions at the zoo: " + e);
        }
        System.out.println(separator);

        // Выведите на экран всех животных из коллекции в порядке возрастания их возраста
        System.out.println("going from youngest to oldest animal:");
        zoo.stream()
                .sorted(Comparator.comparing(Animal::getAge))
                .forEach(animal -> System.out.println(animal.getName() + "; " + animal.getAge() + " years old"));
        System.out.println(separator);

        // Посчитайте сколько животных в коллекции, у которых возраст больше 4
        int count = (int) zoo.stream()
                .filter(animal -> animal.getAge() > 4)
                .count();
        System.out.println("this zoo has " + count + " animals older than 4 years old");
        System.out.println(separator);

        // Создайте Map из коллекции животных. Ключ - название животного, значение - экземпляр животного. Выведите все пары ключ-значение в консоль
        try {
            Map<String, Animal> mappedZoo = zoo.stream()
                    .collect(Collectors.toMap(Animal::getName, animal -> animal));
            System.out.println("the zoo mapped is: " + mappedZoo);
        }
        // проверяем на исключение, потому что в коллекции может быть два и больше животных одного вида, а с одинаковыми ключами мапа не получится:
        catch(IllegalStateException e) {
            System.out.println("the zoo has two or more animals of the same kind: " + e);
            System.out.println("one or more animals will not be accounted for");
            // в таком случае мапа будет использовать для ключа первый вход такого животного и проигнорирует всех остальных с таким же названием
            Map<String, Animal> mappedZoo = zoo.stream()
                    .collect(Collectors.toMap(Animal::getName, animal -> animal, (existing, replacement) -> existing));
            System.out.println("the zoo mapped is: " + mappedZoo);
        }
        System.out.println(separator);

        // Выведите названия всех животных одной строкой (используйте reduce())
        System.out.println("all together now:");
        System.out.println(zoo.stream()
                              .reduce("", (partialString, animal) -> partialString + animal.getName(), String::concat));
        System.out.println(separator);
    }

    // методы для случайного выбора названия животного:
    public static String randomName(ArrayList<String> arrayList) {
        Random random = new Random();
        return arrayList.get(random.nextInt(arrayList.size()));
    }
    // и возраста животного:
    public static int randomAge(int ageBorders) {
        Random random = new Random();
        return random.nextInt(ageBorders);
    }
}